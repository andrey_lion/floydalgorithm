﻿using System;

namespace FloydAlgorithm
{
    class Program
    {
        public const int infinity = 99999;

        public static void FloydAlgorithm(int[,] graph, int countOfColumns)
        {
            int[,] distance = new int[countOfColumns, countOfColumns];

            for (int i = 0; i < countOfColumns; i++)
            {
                for (int j = 0; j < countOfColumns; j++)
                {
                    distance[i, j] = graph[i, j];
                }
            }

            for (int z = 0; z < countOfColumns; z++)
            {
                for (int i = 0; i < countOfColumns; i++)
                {
                    for (int j = 0; j < countOfColumns; j++)
                    {
                        if (distance[i, z] + distance[z, j] < distance[i, j])
                            distance[i, j] = distance[i, z] + distance[z, j];
                    }
                }
            }
            Console.WriteLine("Shortest distances between every pair:");
            PrintGraph(distance, countOfColumns);
        }

        private static void PrintGraph(int[,] distance, int countOfColumns)
        {
            for (int i = 0; i < countOfColumns; i++)
            {
                for (int j = 0; j < countOfColumns; j++)
                {
                    if (distance[i, j] == infinity) Console.Write("{0, 8}", "inf");
                    else Console.Write("{0, 8}", distance[i, j].ToString());
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Test-Case #1:\n");
            int[,] graph1 = {
                { 0,   3,  infinity, 5 },
                { 2, 0, infinity, 4 },
                { infinity, 1, 0, infinity },
                { infinity, infinity, 2, 0 }
            };
            PrintGraph(graph1, 4);
            FloydAlgorithm(graph1, 4);


            Console.WriteLine("Test-Case #2:\n");
            int[,] graph2 = {
                { 0,   3,  infinity, 7 },
                { 8, 0, 2, infinity },
                { 5, infinity, 0, 1 },
                { 2, infinity, infinity, 0 }
            };
            PrintGraph(graph2, 4);
            FloydAlgorithm(graph2, 4);

            Console.WriteLine("Test-Case #3:\n");
            int[,] graph3 = {
                { 0,   5,  infinity, 6 },
                { infinity, 0,   3, infinity },
                { infinity, infinity, 2,   1 },
                { infinity, 1, infinity, 0 }
            };
            PrintGraph(graph3, 4);
            FloydAlgorithm(graph3, 4);
        }
    }
}